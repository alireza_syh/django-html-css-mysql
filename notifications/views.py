from django.contrib.auth.decorators import login_required
from django.db.models.functions import Lower
from django.shortcuts import render, redirect
from .models import Notifications
# Create your views here.

@login_required(login_url='/login/')
def notif(request):
    notifications=Notifications.objects.order_by(Lower('id').desc()).all()
    context={
        'notifications':notifications
    }
    return render(request,'notifications.html',context)

@login_required(login_url='/login/')
def addnotif(request):
    if request.POST.get('submit'):
        subject=request.POST.get('subject')
        text=request.POST.get('text')
        new=Notifications.objects.create(subject=subject,text=text)
        message="added successfull"

    return redirect('notifmain')

@login_required(login_url='/login/')
def editnotif(request,id):
    notification = Notifications.objects.get(id=id)
    if request.POST.get('submit') is not None:
        subject = request.POST.get('subject')
        text = request.POST.get('text')
        ed = Notifications.objects.filter(id=id).update(subject=subject, text=text)

        return redirect('notifmain')
    else:
        context = {
            "notification": notification,
        }
        return render(request, 'editnotif.html', context)

@login_required(login_url='/login/')
def deletenotif(request,id):
    dele = Notifications.objects.get(id=id).delete()
    return redirect("notifmain")