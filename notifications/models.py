from django.db import models

# Create your models here.


class Notifications(models.Model):
    subject = models.CharField(max_length=30)
    text = models.CharField(max_length=150)