from django.apps import AppConfig

class NotificationsConfig(AppConfig):

    label = 'my.notif'