from django.apps import AppConfig

class MainConfig(AppConfig):

    label = 'my.main'