"""wtd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from accounts.views import login2,signup,logout2
from main.views import home,dashboard
from todo.views import todomain,addtodo,deletetodo,edittodo
from notifications.views import notif,addnotif,editnotif,deletenotif


urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/',login2),
    path('signup/',signup),
    path('logout/',logout2),
    path('',home),
    path('dashboard/',dashboard),


    path('todo/',todomain),
    path('edittodo/<id>/',edittodo),
    path('deletetodo/<id>/',deletetodo),
    path('addtodo/',addtodo, name="addtodo"),


    path('notifications/',notif,name="notifmain"),
    path('editnotif/<id>/',editnotif),
    path('deletenotif/<id>/',deletenotif),
    path('addnotif/',addnotif, name="addnotif"),
]
