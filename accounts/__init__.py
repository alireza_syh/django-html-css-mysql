from django.apps import AppConfig

class AccountsConfig(AppConfig):

    label = 'my.accounts'