from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User

# Create your views here.
def login2(request):
    if request.POST.get('submit') is not None:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            redirect="dashboard.html"
            message="u loged in successfully"
        else:
            redirect = "login.html"
            message="invalid creditional"
    else:
        redirect = "login.html"
        message=""

    context={
        'message':message,
    }

    return render(request,redirect,context)

def signup(request):
    if request.POST.get('submit') is not None:
        name=request.POST.get('name')
        email=request.POST.get('email')
        password=request.POST.get('password')
        user=User.objects.create_user(name,email,password)
        if user is not None:
            user2=authenticate(request,username=name,password=password)
            login(request,user2)
            redirect="dashboard.html"
            message="user created successfull and you logged in"

        else:
            redirect="signup.html"
            message="error while createing user"

    else:
        redirect="signup.html"
        message=""

    context={
        'message':message
    }

    return render(request,redirect,context)

def logout2(request):
    logout(request)
    return render(request, "index.html")