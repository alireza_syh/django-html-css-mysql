# Generated by Django 3.0.6 on 2020-05-09 23:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='todo',
            old_name='artist',
            new_name='user',
        ),
    ]
