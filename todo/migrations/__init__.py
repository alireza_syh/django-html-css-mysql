from django.apps import AppConfig

class TodoConfig(AppConfig):

    label = 'my.todo'