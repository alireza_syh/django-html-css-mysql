from django.contrib.auth.decorators import login_required
from django.db.models.functions import Lower
from django.shortcuts import render
from .models import Todo
from django.contrib.auth.models import User
from django.shortcuts import redirect

# Create your views here.

@login_required(login_url='/login/')
def todomain(request):
    todos=Todo.objects.order_by(Lower('id').desc()).filter(user_id=request.user.id)
    users=User.objects.all()
    context={
        'todos':todos,
        'users':users,
    }
    return render(request,'todo.html',context)

@login_required(login_url='/login/')
def addtodo(request):
    if request.POST.get('submit'):
        title=request.POST.get('title')
        user_id=request.POST.get('user')
        new=Todo.objects.create(title=title,user_id=user_id)
        message="added successfull"

    return redirect('/todo/')

@login_required(login_url='/login/')
def edittodo(request,id):
    todo=Todo.objects.get(id=id)
    if request.POST.get('submit') is not None:
        title=request.POST.get('title')
        iscom=request.POST.get('iscom')
        ed=Todo.objects.filter(id=id).update(title=title,is_complate=iscom)

        return redirect('/todo/')
    else:
        context = {
            "todo":todo,
        }
        return render(request, 'edittodo.html', context)


@login_required(login_url='/login/')
def deletetodo(request,id):
    dele=Todo.objects.get(id=id).delete()
    return redirect('/todo/')
